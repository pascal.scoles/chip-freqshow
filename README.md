Chip-FreqShow
========  
See installation and usage instructions in the guide at: https://learn.adafruit.com/freq-show-raspberry-pi-rtl-sdr-scanner/overview



-----
This version is modified to work with the Pocket Chip in it's stock configuration, e.g. with keyboard and touchscreen.


Turns out that there is a bug in the SDL library that pygame relies on in Debian Jessie that causes touch screen mouse input to just go haywire. I initially thought that it was due to the different hardware, but after some digging, found that it is an SDL bug on that platform. There are some workarounds involving downgrading libsdl and so on. 

I'm currently looking into alternatives.


----
Keybindings<br \>
Cancel = c<br \>
Accept = return<br \>
toggle waterfall = t<br \>
fullscreen = f<br \>
settings = s<br \>
num keys = direct<br \>
decimal = .<br \>
+/- toggle = -<br \>
Delete = backspace<br \>
Clear = no binding - use backspace<br \>
Auto = a<br \>
back = ESC<br \>
On settings screen<br \>
center freq = f<br \>
sample rate = r<br \>
gain = g<br \>
min = -<br \>
max = '='<br \>

